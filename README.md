Compile the main sourcefile with GCC as such:
`$ gcc main.c -o sve`

The program can then be used by running
`$ ./sve [0, 1, ...]`
