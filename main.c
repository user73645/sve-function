#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char** argv) {
    
    char* alp[28] = { 
        "A", "B", "C", "D", "E", "F", "G", 
        "H", "I", "J", "K", "L", "M", "N", 
        "O", "P", "Q", "R", "S", "T", "U", 
        "V", "X", "Y", "Z", "Å", "Ä", "Ö" };

    /* Given number, fetch letter (mod 28) */
    for (int i=1; i<argc; i++) {
        int arg = atoi(argv[i])%28;
        printf("%s", alp[arg]); 
    }
    printf("\n"); 
}
